/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.Fraccionario;
import Modelo.Fraccionarios;

/**
 *
 * @author Docente
 */
public class TestFraccionarios {
    
    public static void main(String[] args) {
        Fraccionario x1= new Fraccionario(4,5);
        Fraccionario x2= new Fraccionario(1,4);
        Fraccionario x3= new Fraccionario(4,2);
        Fraccionarios f=new Fraccionarios(new Fraccionario[]{x1,x2,x3});
        System.out.println(f);
        System.out.println("Ordenados:");
        f.sort();
        System.out.println(f);
        
    }
    
}
