package Trabajo.Tests;

public class prueba {

    

        public static void main(String[] args) {
            Integer[] set = {1, 2, 3,4,5};
            printSubsets(set);
        }
    
        public static void printSubsets(Integer[] set) {
            int numSubsets = (int) Math.pow(2, set.length);
            for (int i = 0; i < numSubsets; i++) {
                boolean containsNull = false;
                for (int j = 0; j < set.length; j++) {
                    if ((i & (1 << j)) != 0) {
                        if (set[j] == null) {
                            containsNull = true;
                            break;
                        }
                        System.out.print(set[j] + " ");
                    }
                }
                if (!containsNull) {
                    System.out.println();
                }
            }
        } 
    
}
