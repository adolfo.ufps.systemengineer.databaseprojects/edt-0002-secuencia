package Trabajo.Tests;

import Trabajo.Codes.ListadoMegacorporaciones;
import Trabajo.Codes.Megacorporacion;

/**
 * Test de prueba que simula operaciones entre conjuntos para la clase
 * Megacorporacion
 * 
 * @author Adolfo Alejandro Arenas Ramos (1152370)
 * @author Johan Daniel García Salcedo (1152379)
 */
public class testingMegacorporacion {

    public static void main(String[] args) {
        Megacorporacion apple = new Megacorporacion("Apple", "Tim Cook", "Estados Unidos", "Cupertino", "Cuestionable"),
                microsoft = new Megacorporacion("Microsoft", "Satya Nadella", "Estados Unidos", "Silicon Valley",
                        "Cuestionable"),
                ibm = new Megacorporacion("IBM", "Arvind Krishna", "Estados Unidos", "Nueva York", "No reconocida"),
                tesla = new Megacorporacion("Tesla", "Elon Musk", "Estados Unidos", "Austin", "Buena"),
                blackRock = new Megacorporacion("BlackRock", "Laurence D. Fink", "Estados unidos", "Nueva York",
                        "No reconocida");

        ListadoMegacorporaciones a = new ListadoMegacorporaciones(new Megacorporacion[] { tesla, apple, blackRock });
        ListadoMegacorporaciones b = new ListadoMegacorporaciones(new Megacorporacion[] { ibm, microsoft,tesla});
        ListadoMegacorporaciones c = new ListadoMegacorporaciones(new Megacorporacion[] { ibm, microsoft,tesla, apple, blackRock});

        ListadoMegacorporaciones listaUnion = a.getUnion(b);
        ListadoMegacorporaciones listaInterseccion = a.getInterseccion(b);
        ListadoMegacorporaciones listaDiferenciaA = a.getDiferencia(b);
        ListadoMegacorporaciones listaDiferenciaB = b.getDiferencia(a);
        ListadoMegacorporaciones listaDiferenciaSimetrica = a.getDiferenciaSimetrica(b);

        listaUnion.sort();
        listaInterseccion.sort();
        listaDiferenciaA.sort();
        listaDiferenciaB.sort();
        listaDiferenciaSimetrica.sort();

        System.out.println("\n\n##################\tCONJUNTO A\t####################\n");
        System.out.println(a);

        System.out.println("\n\n##################\tCONJUNTO B\t####################\n");
        System.out.println(b);

        System.out.println("\n\n##################\tUNION\t####################\n");
        System.out.println(listaUnion);

        System.out.println("\n\n##################\tINTERSECCION\t####################\n");
        System.out.println(listaInterseccion);

        System.out.println("\n\n##################\tDIFERENCIA A-B\t####################\n");
        System.out.println(listaDiferenciaA);

        System.out.println("\n\n##################\tDIFERENCIA B-A\t####################\n");
        System.out.println(listaDiferenciaB);

        System.out.println("\n\n##################\tDIFERENCIA SIMETRICA\t####################\n");
        System.out.println(listaDiferenciaSimetrica);

        System.out.println("\n\n##################\tCONJUNTOS UNITARIOS DE A\t####################\n");
        c.getConjuntosPares();
        
    }


}
